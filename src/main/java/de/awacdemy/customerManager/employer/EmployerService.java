package de.awacdemy.customerManager.employer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class EmployerService {
    private EmployerRepository employerRepository;


    @Autowired
    public EmployerService(EmployerRepository employerRepository) {
        this.employerRepository = employerRepository;
    }

    public List<Employer> getEmployerList() {
        return employerRepository.findAll();
    }

    public void add(EmployerDTO employerDTO) {
        Employer employer = new Employer(employerDTO.getName());
        employerRepository.save(employer);
    }

    public Optional <Employer> getEmployer(int Id){
        return employerRepository.findById(Id);
    }
}
