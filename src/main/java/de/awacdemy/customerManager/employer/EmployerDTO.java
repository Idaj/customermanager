package de.awacdemy.customerManager.employer;

import javax.validation.constraints.NotEmpty;

public class EmployerDTO {

    @NotEmpty
    private String name;

    public EmployerDTO(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}

