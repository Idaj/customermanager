package de.awacdemy.customerManager.employer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class EmployerController {

    private EmployerService employerService;

    @Autowired
    public EmployerController(EmployerService employerService) {
        this.employerService= employerService;
    }


    @PostMapping("/addEmployer")
    public String employerSubmit(@Valid @ModelAttribute EmployerDTO employerDTO,
                                 BindingResult bindingresult) {
        if(bindingresult.hasErrors()) {
            return "redirect:/";
        }
        employerService.add(employerDTO);
        return "redirect:/";
    }
}
