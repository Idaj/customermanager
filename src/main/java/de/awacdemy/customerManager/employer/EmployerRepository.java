package de.awacdemy.customerManager.employer;

import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface EmployerRepository extends CrudRepository<Employer, Integer> {
    List<Employer> findAll();
    Optional<Employer> findById(int id);
}
