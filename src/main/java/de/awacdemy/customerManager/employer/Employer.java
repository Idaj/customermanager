package de.awacdemy.customerManager.employer;

import de.awacdemy.customerManager.employee.Employee;

import javax.persistence.*;
import java.util.List;

@Entity
public class Employer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;

    @OneToMany(mappedBy = "employer")
    private List<Employee> employeeList;

    public Employer (){
    }

    public Employer(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Integer getId() {
        return id;
    }

    public List<Employee> getEmployeeList() {
        return employeeList;
    }
}


