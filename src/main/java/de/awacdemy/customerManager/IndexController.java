package de.awacdemy.customerManager;

import de.awacdemy.customerManager.employer.EmployerDTO;
import de.awacdemy.customerManager.employer.EmployerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {

    private EmployerService employerService;

    @Autowired
    public IndexController(EmployerService employerService){
        this.employerService=employerService;
    }

    @GetMapping("/")
    public String clubIndex(Model model) {

        model.addAttribute("employerList", employerService.getEmployerList());
        model.addAttribute("employerDTO", new EmployerDTO(""));
        return "index";
    }
}
