package de.awacdemy.customerManager.employee;

import de.awacdemy.customerManager.employer.Employer;
import de.awacdemy.customerManager.employer.EmployerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class EmployeeController {

    private EmployeeService employeeService;
    private EmployerService employerService;

    @Autowired
    public EmployeeController(EmployeeService employeeService, EmployerService employerService) {
        this.employeeService= employeeService;
        this.employerService = employerService;
    }

    @GetMapping("/addEmployee")
    //value= ... -->hidden input mit th:name = ...
    public String employerOverview(Model model, @ModelAttribute(value = "employerId") Integer employerId){
        Employer employer = employerService.getEmployer(employerId).get();
        model.addAttribute("employer", employer);
        model.addAttribute("employeeDTO", new EmployeeDTO(""));
        return "employerOverview";
    }

    @PostMapping("/addEmployee")
    public String employeeSubmit(@Valid @ModelAttribute EmployeeDTO employeeDTO,
                                 @ModelAttribute(value = "employerId") Integer employerId,
                                 BindingResult bindingresult) {
        if(bindingresult.hasErrors()) {
            return "redirect:/";
        }
        Employee employee = new Employee(employeeDTO.getName());
        //.get() um aus einem optional ein richtiges employer objekt zu erstellen
        Employer employer = employerService.getEmployer(employerId).get();
        employee.setEmployer(employer);
        employeeService.add(employee);
        return "redirect:/";
    }


    @GetMapping("/editEmployee")
    public String employeeEdit(Model model, @ModelAttribute(value = "employeeId") Integer employeeId){
        Employee employee = employeeService.getEmployee(employeeId).get();
        model.addAttribute("employeeDTO", new EmployeeDTO(employee.getName()));
        model.addAttribute("employeeId", employeeId);
        return "edit";
    }


    @PostMapping("/editEmployee")
    public String employeeEditSubmit(@ModelAttribute EmployeeDTO employeeDTO,
                                     @ModelAttribute(value = "employeeId") Integer employeeId) {
        employeeService.edit(employeeDTO, employeeId);
        return "redirect:/";
    }

    @PostMapping("/deleteEmployee")
    public String delete(@ModelAttribute(value= "employeeId") Integer employeeId) {
        employeeService.delete(employeeId);
        return "redirect:/";
    }




}
