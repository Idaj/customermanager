package de.awacdemy.customerManager.employee;

import javax.validation.constraints.NotEmpty;

public class EmployeeDTO {

    @NotEmpty
    private String name;

    public EmployeeDTO(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
