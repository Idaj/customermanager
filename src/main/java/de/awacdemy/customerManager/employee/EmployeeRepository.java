package de.awacdemy.customerManager.employee;

import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface EmployeeRepository extends CrudRepository<Employee, Integer> {
    List<Employee> findAll();



}
