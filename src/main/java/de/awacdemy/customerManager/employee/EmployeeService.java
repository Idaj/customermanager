package de.awacdemy.customerManager.employee;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Optional;

@Service
public class EmployeeService {
    private EmployeeRepository employeeRepository;

    @Autowired
    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }


    public void add(Employee employee) {
        employeeRepository.save(employee);
    }

    public void edit(EmployeeDTO employeeDTO, Integer employeeId) {
        Employee employee = employeeRepository.findById(employeeId).get();
        employee.setName(employeeDTO.getName());
        employeeRepository.save(employee);
    }

    public void delete(Integer employeeId) {
        employeeRepository.deleteById(employeeId);
    }

    public Optional<Employee> getEmployee(Integer employeeId) {
        return employeeRepository.findById(employeeId);
    }
}
